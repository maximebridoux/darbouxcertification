(* ::Package:: *)

(****
MIT License

Copyright (c) 2024
Maxime Bridoux (maxime.bridoux -at- inria.fr)
and 
Khalil Ghorbal (khalil.ghorbal -at- inria.fr)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
****)


BeginPackage["DarbouxCertification`"];


ComputeQuotient::"ComputeQuotient[ode, d, a, morder] Returns the pseudo quotient of the reduction of D(p) w.r.t p with the monomial order morder, assuming that D is encoded by ode, p is Darboux and encoded by d and a "
Coeff::"Coeff[ode, d, a, q, exponent, morder] Returns the coefficient of x^exponent in D(p) - qp, assuming that D is encoded by ode and p is Darboux and encoded by d and a"
LinearPropagation::"LinearPropagation[ode, d, a, q, morder, iterationslimit] Returns the subsitutions obtained for d, p and the associated certificate obtained by scanning coefficients of D(p) - qp, assuming that D is encoded by ode and p is Darboux "
InstanciatePoly::"InstanciatePoly[system, d, a, substd, substp, morder, size] Returns a dense polynomial instanciated with substd and substp"
IsDarbouxPolynomial::"IsDarbouxPolynomial[polynomial, substd, system] Tests whether polynomial is Darboux for the derivation, under the hypothesis given by substd"


Begin["`Private`"]


(*Returns the association {x_i -> list of exponents of the polynomials f_i of the [system]} *)
CoefDic[system_] := ({val} |-> Keys[CoefficientRules[val, Keys[system]]]) /@ system

(*Returns the list of all exponents (given by the association [dicexp]) translated by -1 in the direction of their respective derivative} *)
ProducePoints[dicexp_] := Flatten[MapIndexed[{var, index} |-> ({points} |-> points - Exponent[var, Keys[dicexp]])/@ dicexp[var], Keys[dicexp]], 1]

(* Returns the list of exponents that belong to the Newton polytope of the list [points] *)
SupportQuotient[points_, vars_]:=
	Module[{m = Max[points], n = Length[vars]},
   (Select[Flatten[Array[List, Table[m+1, n], Table[0, n]], n-1],  RegionMember[ConvexHullRegion[points]]])]


(* Removes coefficients a_alpha such that x^alpha > lm(p), according to the monomial order *) 
FilterCoefficientsOrder[expr_, exponentd_, symbola_, morder_]:= Module[{}, (
	expr/. (symbola[n_] /; LexicographicOrder[morder . ((n-exponentd) /. Cases[exponentd, (m_ /; !IntegerQ[m]) :> (m->0)]), Table[0, {i, Length[exponentd]}]] <0 :> 0))]

(* Removes coefficients containing negative entries *)
FilterCoefficientsAbs[expr_, symbola_]:= Module[{}, 
	(expr /. (symbola[n_] /; !AllTrue[n, {x} |-> (!IntegerQ[x]) 
							|| (IntegerQ[x] && GreaterEqualThan[0][x])] :> 0))]
							
(* Apply both filters to eq *)
ApplyFilter[eq_, exponentd_, symbola_, substd_, substp_, morder_] := Module[{}, 
	(Simplify[FilterCoefficientsAbs[(FilterCoefficientsOrder[eq/.substd, exponentd/.substd, symbola, morder]/.substd), symbola]/. substp])]


(* Returns the contribution to c_{exponent} by the elementary opertator [t]*partial x_[index] *)
Contribution[t_, vars_, index_, exponent_, exponentd_, symbola_, morder_] := 
	Module[{n=Length[vars], alpha, coeffp}, (
	Total[Map[{rule}|-> (alpha = exponent - Keys[rule] + Table[If[i==index, 1, 0], {i, n}];
		(* a_alpha = 0 if x^alpha > x^d *)
		coeffp = FilterCoefficientsOrder[symbola[alpha], exponentd, symbola, morder] /. {symbola[exponentd] -> 1};
		If[index===0,
			(* Case index = 0 *)
			Values[rule]*coeffp,
			(* Case index > 0 *)
			Values[rule]*coeffp*alpha[[index]]]), 
		CoefficientRules[t, vars]]])]


(* Implementation of Algorithm 1
	Returns the coefficient c_{[exponent]} of  D(p) - [q]p, where D is encoded by [system]
	Coefficients of p are filtered according to [morder]] *)
Coeff[system_, exponentd_, symbola_, q_, exponent_, morder_] := 
	Module[{vars = Keys[system]},
	(* D(p) *)
	(Total[MapIndexed[{var, index} |-> 
		Contribution[system[var], vars, index[[1]], exponent, exponentd, symbola, morder], 
	vars]] 
	(* - qp *)
		- Contribution[q, vars, 0, exponent, exponentd, symbola, morder])]


Options[ComputeQuotient] = {"SupportQ" -> {}};

(* Implementation of Algorithm 2
Returns q, the generic pseudo quotient of the reduction of D(p) w.r.t p 
 - D is the derivation encoded by [system] 
 - [morder] is the monomial order *)
ComputeQuotient[system_, exponentd_, symbola_, morder_, OptionsPattern[]] := 
	Module[{vars = Keys[system], suppq = OptionValue["SupportQ"], q=0},(
	
	If[Length[suppq] == 0, (* Automatic computation of suppq, if not provided *)
		suppq = SupportQuotient[ProducePoints[CoefDic[system]], vars], Null];
	
	Scan[{exponent} |-> 
		q += Coeff[system, exponentd, symbola, q, exponent+exponentd, morder] * (Times @@ Power[vars, exponent]), 
	(* Main loop on exponents of suppq, sorted by the monomial ordering *)
	Map[Exponent[#, vars]&, MonomialList[Total[Map[Times @@ Power[vars, #]&, suppq]], vars, morder]]]; 
	
	q)]


(* Solve equations==0 given the provided conditions (exponentd is a vector of unknown integers) *) 
FindSol[equations_, exponentd_, conditions_] :=
	Module[{vars, varsd, bigequations}, (
	vars = Complement[Variables[equations], Variables[conditions]];
	varsd = Intersection[vars, exponentd];
	bigequations = Join[{equations==0}, (* original equations to solve *)
				Table[v \[Element] NonNegativeIntegers, {v, varsd}], (* the components of d are non-negative integers *)
				conditions]; (* additional conditions *)
	Map[Simplify[#, Assumptions->Join[Table[v \[Element] NonNegativeIntegers, {v, varsd}], conditions]]&, List[ToRules[Reduce[bigequations, vars]]]])]


(* Produces {a list of {exponent, substitution for d}, 
			 a list of {exponent, substitution for p}} *) 
ConstantPropagation[rules_, exponentd_, symbola_, conditions_]:=
	Module[{currentSubstd = {}, sol, i},(
	{Flatten[
	Cases[Normal[rules],  (key_ -> (eq_)) /; (FreeQ[eq/.currentSubstd, symbola[_]]&& !((eq/.currentSubstd)===0)) :> 
		If[(eq/.currentSubstd)===0, {key, {}},
		(Print["Found equation ",eq/.currentSubstd, "==0 at ", Position[rules, eq, 1][[1]][[1]]];
		sol=FindSol[(eq/.currentSubstd), exponentd, conditions];
		Switch[Length[sol], 
			0, Print["System has no solution"]; Break[], 
			1, Print["Solution: ", sol[[1]]]; currentSubstd = Join[currentSubstd, sol[[1]]]; {key, sol[[1]]}, 
			_, Print["Several solutions are available: ", sol];
				i=ChoiceDialog["Several solutions are available. Please choose a solution to explore: ", MapIndexed[{sub, index} |-> sub->index[[1]],sol]];
				If[IntegerQ[i], Null, i=1];Print["Chosen solution: ", sol[[i]]]; currentSubstd = Join[currentSubstd, sol[[i]]]; {key, sol[[i]]}])]], 0], 
	(* Linear case *)
	Cases[Expand[Normal[rules], symbola[_]], (key_ -> div_.*symbola[n_]+coef_.) 
		/; FreeQ[div, symbola[_]] && FreeQ[coef, symbola[_]] && (!(div==0) || (Length[FindSol[div, exponentd, conditions]]==0)) 
		:> {key, symbola[n]-> Simplify[-coef/div]}]
	})]


(* Returns the list of points d+d' for |d'| <= [size]. We use the infinity norm. *)
MakeBox[exponentd_, size_]:=
	Module[{n = Length[exponentd]}, Flatten[Array[{##}&, Table[2*size+1, n], Table[exponentd[[i]]-size, {i, n}]],n-1]]

(* Computes the coefficient c_beta for beta in [box] and adds it to [equations], or updates it if it is already present *)
ScanEquations[normalformfun_, exponentd_, symbola_, morder_, substd_, substp_, equations_, box_]:=
	Module[{eqs = equations}, (
	Scan[{point} |-> If[KeyExistsQ[equations, point], 
		(* Update equation it is stored *)
		eqs[point] = ApplyFilter[eqs[point], exponentd, symbola, substd, substp, morder], 
		(* Otherwise compute it and store it *)
		AssociateTo[eqs, point -> ApplyFilter[normalformfun[point], exponentd, symbola, substd, substp, morder]]
	], box]; eqs)]

(* Returns the new values of [substd] and [substd] according to [certificateD] and [certificateP] *)
UpdateSubstitutions[substd_, substp_, certificateD_, certificateP_] := 
	Module[{newsubstd, newsubstp},(
	newsubstd = Join[substd, Flatten[Map[#[[2]]&, certificateD], 1]]; 
	newsubstp = Join[substp, Flatten[Map[#[[2]]&, certificateP], 1]];
	newsubstp = Map[#[[1]] -> (#[[2]] /. newsubstp)&, newsubstp] /. newsubstd;
	{newsubstd, newsubstp})]


(* Core loops of Algorithm 3 *)
IteratePropagation[system_, exponentd_, symbola_, quotient_, morder_, iterationslimit_, 
	initialsubstd_, initialsubstp_, conditions_]:=
	Module[{substd=initialsubstd, substp=initialsubstp/.initialsubstd, equations = Association[], certificate = {}, r=0, box, loop, certificateD, certificateP}, (
	
	For[r=0, r<=Max[Abs[iterationslimit]], r++, 
		box = MakeBox[exponentd, r];
		loop = True;
		While[loop; 
			loop = False;
			
			(* store and update equations based on the current substitution *)
			equations = ScanEquations[Coeff[system, exponentd, symbola, quotient, #, morder]&, exponentd, symbola, morder, substd, substp, equations, box];
			(* search for new substitutions *)
			{certificateD, certificateP} = ConstantPropagation[equations, exponentd, symbola, conditions];
			
			If[Length[certificateD]>0 || Length[certificateP]>0, 
				(* new substitutions have been found *)
				loop = True; certificate = Join[certificate, certificateP, certificateD], Null];
			
			(* updates the current substitutions *)
			{substd, substp} = UpdateSubstitutions[substd, substp, certificateD, certificateP]]
		]; 
	{substd, 
	Select[Map[FilterCoefficientsAbs[#, symbola]&, substp], !(#[[1]]===0)&], (* Coefficients of p with negatives components are filtered out *)
	certificate})]


Options[LinearPropagation] = {"Substd" -> {}, (* Initial substitution for d *)
							 "Substp" -> {},  (* Initial substitution for p *)
							 "Conditions"-> {}};  (* List of conditions on the parameters of the ODE *)
							 
(* Implementation of Algorithm 3 *)
LinearPropagation[system_, exponentd_, symbola_, quotient_, morder_, iterationslimit_, OptionsPattern[]] := 
	Module[{}, (
	IteratePropagation[system, exponentd, symbola, quotient, morder, iterationslimit, OptionValue["Substd"], Join[{symbola[exponentd]->1}, OptionValue["Substp"]], OptionValue["Conditions"]])]


(* Computes D(p) for a concrete [p] where D is the derivation related to [system] *)
Diff[p_, system_]:=Dot[Values[system],D[p,{Keys[system]}]]

(* Returns True iff the polynomial [polynomial] is Darboux for the [system], under the hypothesis given by [substd] *)
IsDarbouxPolynomial[polynomial_, substd_, system_]:= Module[{quop, remp}, (
	{{quop}, remp} =PolynomialReduce[Diff[polynomial, system], polynomial, Keys[system]];
	((remp/. substd) === 0))]


(* Produces a list of rules (exponent -> a[exponent]) for exponents of shape d+d', with |d'| <= size *)
GeneralPrototype[exponentd_, symbola_, size_]:=(Map[{exp}|->(exp -> symbola[exp]), MakeBox[exponentd, size]])

(* Produces a concrete polynomial by instanciating the coefficients (according the the substitutions) of monomials of shape x^{d+d'}, with |d'| <= size *)
InstanciatePoly[system_, exponentd_, symbola_, substd_, substp_, morder_, size_]:=Module[{}, (
	FromCoefficientRules[ApplyFilter[GeneralPrototype[exponentd, symbola, size], exponentd, symbola, substd, substp/.substd, morder], Keys[system]])]


End[]
EndPackage[]
