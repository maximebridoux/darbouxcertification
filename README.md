# DarbouxCertification

DarbouxCertification is a Mathematica package for making explicit the potential dependencies between the exponent of the leading monomial of a candidate Darboux polynomial, and the polynomials defining the ODE.

## Installation

You can use the Mathematica menu item ``File -> Install..`` to install the file ``DarbouxCertification.wl`` as a package.
See also the [official Wolfram documentation](https://support.wolfram.com/5648) for installing packages.

## Examples

This package includes several notebooks which illustrate how to use the provided functions : 
 - ``run_vdp.nb`` contains an automated proof for the nonexistence of nontrivial Darboux polynomial for the Van der Pol Oscillator 
 - ``run_lienard.nb`` highlights how to extract conditions on the polynomials defining the ODE for a Darboux polynomial to exist for a system belonging to the Liénard class
 - ``run_rossler.nb`` features the Rössler system of dimension 3

## References

1) A Mathematica Package for Certifying the Nonexistence of Darboux Polynomials, M. Bridoux, K. Ghorbal
2) Automated Reasoning For The Existence Of Darboux Polynomials, K. Ghorbal, M. Bridoux
